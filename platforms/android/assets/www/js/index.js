var pictureSource;   // picture source
var destinationType; // sets the format of returned value

// Wait for device API libraries to load
document.addEventListener("deviceready", onDeviceReady,false);

// device APIs are available
function onDeviceReady() {
    pictureSource   = navigator.camera.PictureSourceType;
    destinationType = navigator.camera.DestinationType;

    jQuery('#snapMeNow').click( function() {
        capturePhoto();
    });
}

// Called when a photo is successfully retrieved
function onPhotoDataSuccess(imageData) {

    // Unhide image elements
    jQuery('#largeImage')
        // Show the captured photo
        .attr('src', "data:image/jpeg;base64," + imageData)
        // Unhide image elements
        .show();

    jQuery('#result').show();
}

// A button will call this function
function capturePhoto() {

    // Clear previous image
    clearImages();

    // Take picture using device camera, allow edit, and retrieve image as base64-encoded string
    navigator.camera.getPicture(onPhotoDataSuccess, onFail,
        {
            quality: 90,
            allowEdit: false,
            destinationType: destinationType.DATA_URL
        }
    );
}

// Clear previous image
function clearImages() {
    jQuery('#largeImage, #smallImage')
        .hide()
        .attr('src', '');

    jQuery('#result').hide();
}

// Called if something bad happens.
function onFail(message) {
    alert('Failed because: ' + message);
}